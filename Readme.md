## About the book:

- About the book (https://gitlab.com/aesthetic-programming/book/-/tree/master/source)
- Table of contents (https://gitlab.com/aesthetic-programming/book/-/blob/master/source/Table_Of_Contents.md)
- Preface (https://gitlab.com/aesthetic-programming/book/-/tree/master/source/0-Preface)
- Ch.1: Getting started (https://gitlab.com/aesthetic-programming/book/-/tree/master/source/1-GettingStarted)
- Ch.2: Variable geometry (https://gitlab.com/aesthetic-programming/book/-/tree/master/source/1-GettingStarted)
- Ch.3: Infinite loops (https://gitlab.com/aesthetic-programming/book/-/tree/master/source/3-InfiniteLoops)
- Ch.4: Data capture (https://gitlab.com/aesthetic-programming/book/-/tree/master/source/4-DataCapture) 
- Ch.5: Auto-generator (https://gitlab.com/aesthetic-programming/book/-/tree/master/source/5-AutoGenerator)
- Ch.6: Object abstraction (https://gitlab.com/aesthetic-programming/book/-/tree/master/source/6-ObjectAbstraction)
- Ch.7: Vocable code (https://gitlab.com/aesthetic-programming/book/-/tree/master/source/7-VocableCode)
- Ch.8: Que(e)ry data (https://gitlab.com/aesthetic-programming/book/-/tree/master/source/8-Que(e)ryData)
- Ch.9: Algorithmic procedures (https://gitlab.com/aesthetic-programming/book/-/tree/master/source/9-AlgorithmicProcedures)
- Ch.10: Machine unlearning (https://gitlab.com/aesthetic-programming/book/-/tree/master/source/10-MachineUnlearning)
- Afterword: Recurrent Imaginaries (https://gitlab.com/aesthetic-programming/book/-/tree/master/source/11-Afterword_RecurrentImaginaries)
- Ch.12: Literary_critics_programming_languages (https://gitlab.com/softwareplay/aesthetic-programming/-/tree/master/source/12-Literary_critics_programming_languages)
- Selected student projects (https://gitlab.com/aesthetic-programming/book/-/blob/master/source/showcase.md)
- Acknowledgements (https://gitlab.com/aesthetic-programming/book/-/blob/master/source/acknowledgments.md)

## The sample code (public folder):

RunMe: https://aesthetic-programming.gitlab.io/book/

Repository: https://gitlab.com/softwareplay/aesthetic-programming/-/tree/master

## All the flowchart (graphviz folder):

Source: https://gitlab.com/aesthetic-programming/book/-/tree/master/graphviz


